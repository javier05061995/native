create DATABASE todo_tutorial;
CREATE TABLE users(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(150),
    email VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(255)
);
CREATE TABLE todos(
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(150),
    completed BOOLEAN DEFAULT false,
    user_id INT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);
CREATE TABLE shared_todos(
    id INT AUTO_INCREMENT PRIMARY KEY,
    todo_id INT,
    user_id INT,
    shared_with_id INT,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY (todo_id) REFERENCES todos(id) ON DELETE CASCADE,
    FOREIGN KEY (shared_with_id) REFERENCES users(id) ON DELETE CASCADE
);
INSERT INTO users (name, email, password) value ('JAVIER', 'nnyxv@gmail.com', '123123');
INSERT INTO users (name, email, password) value ('miguel', 'nnyxv123@gmail.com', '123123');
INSERT INTO todos(title, user_id)
VALUES ("ir al gym", 1),
    ("llegar a bañarse", 1),
    ("desayunar ", 1),
    ("salir a trabajar ", 1),
    ("almorzar", 1),
    ("salir del trabajo", 1),
    ("cenar", 1),
    ("dormir", 1);
INSERT into shared_todos (todo_id, user_id, shared_with_id)
VALUES(1, 1, 2);
select todos.*,
    shared_todos.shared_with_id
from todos
    Left join shared_todos ON todos.id = shared_todos.todo_id
where todos.user_id = [user_id]
    or shared_todos.shared_with_id = [user_id];