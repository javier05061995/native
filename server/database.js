import mysql from "mysql2";
import dotenv from "dotenv";
dotenv.config();

const pool = mysql
  .createPool({
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
  })
  .promise();

export async function getTodosByID(id) {
  const [row] = await pool.query(
    `select todos.*,shared_todos.shared_with_id
    from todos
    Left join shared_todos ON todos.id = shared_todos.todo_id
    where todos.user_id = ? or shared_todos.shared_with_id = ?`,
    [id, id]
  );
  return row;
}

export async function getTodo(id) {
  const [row] = await pool.query(`SELECT * FROM todos WHERE id = ?`, [id]);
  return row[0];
}
export async function getSharedTodoByID(id) {
  const [row] = await pool.query(
    `SELECT * FROM shared_todos WHERE todo_id = ?`,
    [id]
  );
  return row[0];
}

export async function getUserByID(id) {
  const [row] = await pool.query(`SELECT * FROM users WHERE id = ?`, [id]);
  return row[0];
}

export async function getUserByEmail(email) {
  const [rows] = await pool.query(`SELECT * FROM users where email = ? `, [
    email,
  ]);
  return rows[0];
}
export async function createTodo(user_id, title) {
  const [result] = await pool.query(
    `INSERT into todos (user_id, title) VALUE(?,?)`,
    [user_id, title]
  );
  const todoID = result.insertId;

  return getTodo(todoID);
}

export async function deleteTodo(id) {
  const [result] = await pool.query(`DELETE from todos where id = ?`, [id]);
}

export async function toggleCompleted(id, value) {
  const new_value = value == true ? "TRUE" : "FALSE";
  const [result] = await pool.query(
    `
    UPDATE todos set completed = ${new_value}
    where id = ?
        
        `,
    [id]
  );
  return result;
}

export async function shareTodo(todo_id, user_id, shared_with_id) {
  const [result] = await pool.query(
    `
            INSERT INTO shared_todos (todo_id , user_id, shared_with_id)
            value( ? , ?  ?)
        `,
    [todo_id, user_id, shared_with_id]
  );
  return result.insertId;
}
