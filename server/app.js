import express from "express";
import {
  getTodo,
  deleteTodo,
  getTodosByID,
  createTodo,
  toggleCompleted,
  getUserByEmail,
  getUserByID,
  getSharedTodoByID,
  shareTodo,
} from "./database.js";
import cors from "cors";

const corsOptions = {
  methods: ["POST", "GET"],
  credentials: true,
};

const app = express();
app.use(express.json());
app.use(cors(corsOptions));

app.get("/todos/:id", async (req, res) => {
  const todos = await getTodosByID(req.params.id);
  res.status(200).send(todos);
});

app.get("/todos/shared_todos/:id", async (req, res) => {
  const todo = await getSharedTodoByID(req.params.id);
  const author = await getUserByID(todo.id);
  const shared_with = await getUserByEmail(todo.shared_with_id);
  res.status(200).send({ author, shared_with });
});

app.get("/users/:id", async (req, res) => {
  const user = await getUserByID(req.params.id);
  res.status(200).send(user);
});

app.put("/todos/:id", async (req, res) => {
  const { value } = req.body;
  const todos = await toggleCompleted(req.params.id, value);
  res.status(200).send(todos);
});
app.delete("/todos/:id", async (req, res) => {
  const todos = await deleteTodo(req.params.id);
  res.status(200).send({ message: "eliminado correctamente" });
});

app.post("/todos/shared_todos", async (req, res) => {
  const { todo_id, user_id, email } = req.body;
  const userToShare = await getUserByEmail(email);
  const shareTodos = await shareTodo(todo_id, user_id, userToShare.id);
  res.status(201).send(shareTodos);
});

app.post("/todos", async (req, res) => {
  const { user_id, todos } = req.body;
  const todo = await createTodo(user_id, todos);
  res.status(201).send(todo);
});

app.listen(8080, () => {
  console.log("server corriendo en el puerto 8080");
});
